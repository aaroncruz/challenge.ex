defmodule Challenge.PageScraper do
  def page(link) do
    extract_html(link)
    |> make_absolute_paths
  end

  defp extract_html(url) do
    HTTPoison.start
    %{url: url, html: HTTPoison.get!(url)}
  end

  defp make_absolute_paths(page_data) do
    %{html: %{body: raw}, url: url} = page_data
    doc = Floki.parse(raw)
    html = parse_html_nodes(doc, url)
    with_canonical = add_canonical(html, url)
    Floki.raw_html(with_canonical)
  end

  defp add_canonical(html, url) do
    {"html", attr, children} = html
    new_children = Enum.map(children, fn(child) ->
      case child do
        {"head", attr, c} -> {"head", attr, with_original_link(c, url)}
        _ -> child
      end
    end)
    {"html", attr, new_children}
  end

  defp with_original_link(elements, url) do
    el = {"link", [{"href", url}, {"rel", "canonical"}], []}
    [el|elements]
  end

  defp parse_html_nodes({tag_name, attributes, children_nodes}, url) do
    # replace attributes
    new_attributes = Enum.map(attributes, &(replace_elements(&1, url)))
    # recurse with children_nodes
    new_children = Enum.map(children_nodes, &(parse_html_nodes(&1, url)))
    {tag_name, new_attributes, new_children}
  end
  defp parse_html_nodes({tag_name, content}, _url) do
    {tag_name, content}
  end
  defp parse_html_nodes(text_node, _url) when is_bitstring(text_node) do
    text_node
  end

  defp replace_elements({"href", url}, base_url) do
    {"href", make_absolute(url, base_url)}
  end
  defp replace_elements({"src", url}, base_url) do
    {"src", make_absolute(url, base_url)}
  end
  defp replace_elements(attr, _base_url) do
    attr
  end

  defp make_absolute(url, base_url) do
    cond do
      String.match?(url, ~r/https?:\/\//) ->
        url
      # relative to base
      String.match?(url, ~r/^\//) ->
        base = get_base_url(base_url)
        base <> url
      # Link within page
      String.match?(url, ~r/^#/) ->
        url
      # relative to page
      true -> 
        get_relative_url(url, base_url)
    end
  end

  defp get_relative_url(url, base_url) do
    parts = String.split(base_url, "/")
    first_parts = Enum.drop(parts, -1)
    base = Enum.join(first_parts, "/")
    base <> "/" <> url
  end

  defp get_base_url(url) do
    matches = Regex.named_captures(~r/(?<base>https?:\/\/.+?)\//, url)
    matches["base"]
  end
end
