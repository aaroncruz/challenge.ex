defmodule Challenge.LinkExtractor do
  def links do
    priv = Path.expand("../../../priv/data/links.yml", __ENV__.file)
    YamlElixir.read_from_file(priv)
  end
end
