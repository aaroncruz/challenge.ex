defmodule Challenge.LinkUpdater do
  def start_link do
    spawn_link run
  end

  defp run do
    IO.puts("Start scraping...")
    update_links
    IO.puts("Done scraping...")
    :timer.sleep(delay)
    run
  end

  defp delay do
    d = System.get_env("POLL_DELAY_SECONDS")
    case d do
      nil -> (60 * 60) * 1000 # wait one hour default
      _ -> Integer.parse(d) |> elem(0)
    end
  end

  defp update_links do
    Challenge.LinkExtractor.links
    |> Enum.map(&extract_page/1)
    |> Enum.map(&Challenge.PageServer.save/1)
  end

  defp extract_page({shortlink, url}) do
    %{short_link: shortlink,
      url: url,
      html: Challenge.PageScraper.page(url)}
  end
end
